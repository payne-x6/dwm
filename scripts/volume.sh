#!/bin/bash

# Prints the current volume or 🔇 if muted.

#case $BLOCK_BUTTON in
#	1) setsid -f "$TERMINAL" -e pulsemixer ;;
#	2) pamixer -t ;;
#	4) pamixer --allow-boost -i 1 ;;
#	5) pamixer --allow-boost -d 1 ;;
#	3) notify-send "📢 Volume module" "\- Shows volume 🔊, 🔇 if muted.
#- Middle click to mute.
#- Scroll to change." ;;
#	6) "$TERMINAL" -e "$EDITOR" "$0" ;;
#esac


vol=( `awk -F"[][]" '/dB/ { print $2, $6 }' <(amixer sget Master) | sed 's/%//g'` )

[ ${vol[1]} = off ] && echo 🔇 && exit

if [ "${vol[0]}" -gt "70" ]; then
	icon="🔊"
elif [ "${vol[0]}" -lt "30" ]; then
	icon="🔈"
else
	icon="🔉"
fi

echo "$icon${vol[0]}%"

