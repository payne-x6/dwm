#!/usr/bin/env sh

case `xset -q | grep LED | awk '{print $10}'` in
	"00000002") echo "CZ" ;;
	"00001002") echo "EN" ;;
esac
