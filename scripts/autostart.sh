#!/usr/bin/env bash

dwmblocks &
nitrogen --restore
setxkbmap -model pc104 -layout cz,us -variant qwerty, -option grp:win_space_toggle


[ -f ~/.dwm/autostart.sh ] && source ~/.dwm/autostart.sh
